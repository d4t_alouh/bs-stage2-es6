import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let keysPressed = {};
    const gameState = {
      firstPlayer: {
        ...firstFighter,
        currentHealth: firstFighter.health,
        blocked: false,
        bar: document.getElementById('left-fighter-indicator'),
        prevCriticalHitTime: 0
      },
      secondPlayer: {
        ...secondFighter,
        currentHealth: secondFighter.health,
        blocked: false,
        bar: document.getElementById('right-fighter-indicator'),
        prevCriticalHitTime: 0
      }
    };
    document.addEventListener('keydown', (event) => {
      const keyPressed = event.code;
      keysPressed[keyPressed] = true;
      // handle classical hit buttons
      if (keyPressed === controls.PlayerOneBlock) {
        gameState.firstPlayer.blocked = true;
      } else if (keyPressed === controls.PlayerTwoBlock) {
        gameState.secondPlayer.blocked = true;
      } else if (keyPressed === controls.PlayerOneAttack) {
        hitPlayer(gameState.firstPlayer, gameState.secondPlayer);
      } else if (keyPressed === controls.PlayerTwoAttack) {
        hitPlayer(gameState.secondPlayer, gameState.firstPlayer);
      }
      // handle sequence critical hit
      if (controls.PlayerOneCriticalHitCombination.every(key => key in keysPressed)) {
        criticalHitPlayer(gameState.firstPlayer, gameState.secondPlayer);
      } else if (controls.PlayerTwoCriticalHitCombination.every(key => key in keysPressed)) {
        criticalHitPlayer(gameState.secondPlayer, gameState.firstPlayer);
      }
      // handle win
      if (gameState.firstPlayer.currentHealth <= 0) {
        resolve(secondFighter);
      } else if (gameState.secondPlayer.currentHealth <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      const keyPressed = event.code;
      delete keysPressed[keyPressed];
      if (keyPressed === controls.PlayerOneBlock) {
        gameState.firstPlayer.blocked = false;
      } else if (keyPressed === controls.PlayerTwoBlock) {
        gameState.secondPlayer.blocked = false;
      }
    });

  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return hitPower > blockPower ? hitPower - blockPower : 0;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  const { attack } = fighter;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  const { defense } = fighter;
  return defense * dodgeChance;
}

export function hitPlayer(attacker, defender) {
  if (!attacker.blocked && !defender.blocked) {
    let damage = getDamage(attacker, defender);
    console.log('Player dealt ', damage);
    defender.currentHealth -= damage;
    defender.bar.style.width = (defender.currentHealth * 100) / defender.health + '%';
  }
}

export function criticalHitPlayer(attacker, defender) {
  let currentTime = Date.now();
  console.log(currentTime - attacker.prevCriticalHitTime)
  if (currentTime - attacker.prevCriticalHitTime >= 10000) {
    attacker.prevCriticalHitTime = currentTime;
    let damage = getHitPower(attacker);
    console.log('Player dealt critical ', 2 * damage);
    defender.currentHealth -= 2 * damage;
    defender.bar.style.width = (defender.currentHealth * 100) / defender.health + '%';
  }
}
